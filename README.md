# k8s-webapps

A combination of bash scripts, Kubernetes resource definitions,
and helm charts for deploying several stateless apps

It uses k3s with a traefik ingress and installs cert manager
to easily manage ACME TLS certificates
Helm is used to deploy new versions of the target applicatons

install.sh - Performs an initial installation of k3s plus any
             k8s resources which must be installed to specific
             namespaces / global scope

apply.sh   - Installs stateless applications using helm
