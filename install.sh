# Install Scaleway-specific ingress controller
kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/controller-v0.44.0/deploy/static/provider/scw/deploy.yaml

# Install hairpin proxy to work around https://github.com/jetstack/cert-manager/issues/3341
kubectl apply -f https://raw.githubusercontent.com/compumike/hairpin-proxy/v0.1.2/deploy.yml

# Install cert manager
kubectl apply --validate=false -f https://github.com/jetstack/cert-manager/releases/download/v0.16.1/cert-manager.yaml

# Install staging/prod issuers
kubectl apply -f issuer.yaml

# Get ready to deploy apps
kubectl create ns apps

./apply.sh
