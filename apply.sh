kubens apps

# Deploy all apps
cd environment

helm dependency update
helm upgrade -i apps .

cd ..
